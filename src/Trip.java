
public class Trip {
	

    private String[] zone1 = {"Don Mills", "Leslie"};
    private String[] zone2 = {"Finch", "Sheppard"};

    private double zone1Price = 2.5;
    private double zone2Price = 3.0;
    private double zone1DailyMax = 5.50;
    private double zone2DailyMax = 6.00;
	
    public double calculateTotal(String[] from, String[] to) {
        double totalCost = 0.0;

	
        for(int index = 0; index < from.length; index++) {
           
            if(doesArrayHasElement(zone1, from[index]) && doesArrayHasElement(zone1, to[index])) {
                totalCost += zone1Price;
            }
           
            else if (doesArrayHasElement(zone2, from[index]) && doesArrayHasElement(zone2, to[index])) {
                totalCost += zone2Price;
            }
        
            else if ((doesArrayHasElement(zone1, from[index]) && doesArrayHasElement(zone2, to[index])) ||
                    (doesArrayHasElement(zone2, from[index]) && doesArrayHasElement(zone1, to[index]))) {
                totalCost += 5;
            }
        
            if(totalCost > zone2DailyMax) {
                totalCost = 6.0;
            }

           
        }

        return totalCost;
    }



private boolean doesArrayHasElement(String[] array, String element) {
    boolean isPresent = false;

    for(int index = 0; index < array.length; index++) {
        if(array[index].equalsIgnoreCase(element)) {
            isPresent = true;
            break;
        }
    }

    return isPresent;
}
}



  
    
