import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TripTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test 
	public void calculateZone1Trip() {
        Trip trip = new Trip();
        double cost;

        cost = trip.calculateTotal(new String[]{"Leslie"}, new String[]{"Don Mills"});
		
		
        assertEquals(cost, 2.5,0);
    }

	
	@Test
	public void calculateZone2Trip() {
        Trip trip = new Trip();
        double cost;

        cost = trip.calculateTotal(new String[]{"Sheppard"}, new String[]{"Finch"});
        assertEquals(cost, 3, 0);
    }

	
	@Test 
	public void calculateBetweenZone() {
        Trip trip = new Trip();
        double cost;

        cost = trip.calculateTotal(new String[]{"Don Mills"}, new String[]{"Finch"});
        assertEquals(cost, 5, 0);
    }
	
    @Test 
    
    public void calculateMoreThanOneTrip() {
        Trip trip = new Trip();
        double cost;

        cost = trip.calculateTotal(new String[]{"Finch", "Leslie"}, new String[]{"Sheppard", "Don Mills"});
        assertEquals(cost, 5, 0);
    }



}
